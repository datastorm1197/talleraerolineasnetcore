﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class AerolineaContext: DbContext
    {
        public AerolineaContext(DbContextOptions<AerolineaContext> options): base(options)
        {

        }

        public DbSet<Aerolinea> Aerolineas { get; set; }
    }
}
