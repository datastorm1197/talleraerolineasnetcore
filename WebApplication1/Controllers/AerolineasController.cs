﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AerolineasController : ControllerBase
    {
        private readonly AerolineaContext _context;

        public AerolineasController(AerolineaContext context)
        {
            _context = context;
        }

        // GET: api/Aerolineas // GET - POST - PUT - DELETE
        [HttpGet]
        public IEnumerable<Aerolinea> GetAerolineas()
        {
            return _context.Aerolineas;
        }

        // GET: api/Aerolineas/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAerolinea([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var aerolinea = await _context.Aerolineas.FindAsync(id);

            if (aerolinea == null)
            {
                return NotFound();
            }

            return Ok(aerolinea);
        }

        // PUT: api/Aerolineas/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAerolinea([FromRoute] int id, [FromBody] Aerolinea aerolinea)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != aerolinea.Id)
            {
                return BadRequest();
            }

            _context.Entry(aerolinea).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AerolineaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Aerolineas
        [HttpPost]
        public async Task<IActionResult> PostAerolinea([FromBody] Aerolinea aerolinea)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Aerolineas.Add(aerolinea);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAerolinea", new { id = aerolinea.Id }, aerolinea);
        }

        // DELETE: api/Aerolineas/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAerolinea([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var aerolinea = await _context.Aerolineas.FindAsync(id);
            if (aerolinea == null)
            {
                return NotFound();
            }

            _context.Aerolineas.Remove(aerolinea);
            await _context.SaveChangesAsync();

            return Ok(aerolinea);
        }

        private bool AerolineaExists(int id)
        {
            return _context.Aerolineas.Any(e => e.Id == id);
        }
    }
}